Pour réaliser une addition, il suffit de prendre plusieurs nombres et d'ajouter leur valeur.
Mais comment une addition est-elle programmée ?
Il suffit de créer une fonction addition, qui additionnera les différents nombres données en paramètre.
Tout d'abord la création de la fonction:

def addition (nombre1,nombre2):
    '''
    cet algoritme aditionne un nombre a un autre.
    '''

Ici nombre1 et nombre2 seront les deux nombres additionnées, appelés paramètres.
Ce sont des variables, ce qui signifie que l'on peut leur donner n'importe quel valeur.

On voit ensuite le corps du code: 

res = nombre1 + nombre2#ici res prend la valeur de l'addition des deux nombres

Et on renvoie le résultat.

return res#ici on renvoie le résultat

Voici la fonction en entier : 

def addition (nombre1,nombre2):
    '''
    cet algoritme aditionne un nombre a un autre.
    '''
    res = nombre1 + nombre2#ici res prend la valeur de l'addition des deux nombres
    return res#ici on renvoie le résultat

Pour appeler la fonction, il suffit de faire :
addition(3,5)
Ce qui renvoie le bon résultat:
8
