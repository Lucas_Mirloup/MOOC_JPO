<?php
$all=False;
if ($_GET["v1"]==3 &&
    $_GET["v2"]==2 &&
    $_GET["v3"]==3){
    $all=True;}?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="../css/materialize.min.css" rel="stylesheet">
    <link href="../css/answers.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Additions en Python</title>
</head>
<body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../js/materialize.min.js"></script>
<script>$(document).ready(function() {$('select').material_select();});</script>
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper blue darken-2">
            <ul class="left hide-on-med-and-down">
                <li><a href="../index.html"><i class="left material-icons">home</i>Accueil</a></li>
                <li><a href="additions.html">Additions & Fonctions</a></li>
                <li class="active"><a href="longueur.html">Longueur de chaîne</a></li>
                <li><a href="voyelles.html">Voyelles dans un mot</a></li>
                <li><a href="discriminant.html">Discriminant</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="header center blue-text">
    <h1>Additions & Fonctions en Python - Résultat</h1>
</div>
<section style="margin:0 1vw 1vw 1vw">
    <div class="row">
        <h4 class="center">La correction est affichée sur cette page.</h4>
        <p class="flow-text center">Pour chaque champ, si votre réponse était bonne, le champ sera <span style="color:forestgreen">vert</span>, si elle était fausse, il sera <span style="color:red">rouge</span>.</p>
    </div>
<div>
    <div class="row">
        <p class="col s1 flow-text">
            def
        </p>
        <p class="col s1 flow-text">
            longueur(
        </p>
        <p class="col s1 flow-text right-align">
            chaine
        </p>
        <p class="col s1 flow-text">
            ):
        </p>
    </div>
    <div class="row">
        <p class="col s1 offset-s1 flow-text <?php if($_GET["v1"]==3){echo "good";}else{echo "bad";}?>">
            return
        </p>
        <p class="col s1 flow-text <?php if($_GET["v2"]==2){echo "good";}else{echo "bad";}?>">
            len(
        </p>
        <p class="col s1 flow-text <?php if($_GET["v3"]==3){echo "good";}else{echo "bad";}?>">
            chaine
        </p>
        <p class="col s1 flow-text">
            )
        </p>
    </div>
</div>
<h4 class="center <?php if($all){echo "goodend";}else{echo "badend";}?>">
    <?php
    if ($all)
    {echo "Félicitations, vous avez correctement répondu.";}
    else{echo "Vous avez une ou plusieurs erreurs, vous ferez mieux la prochaine fois.";}
    ?></h4>
</section>
</body>
</html>